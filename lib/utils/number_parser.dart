import 'package:math_expressions/math_expressions.dart';

String numberParser(String expression) {
  final Parser parser = Parser();
  final Expression exp = parser.parse(expression);
  final ContextModel contextModel = ContextModel();
  return exp.evaluate(EvaluationType.REAL, contextModel).toString();
}
