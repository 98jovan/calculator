import 'package:calculator/bloc/calculator_bloc.dart';
import 'package:calculator/repository/history_repository.dart';
import 'package:calculator/screens/calculator_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Provider<HistoryRepository>(
        create: (context) => HistoryRepository(),
        child: RxBlocProvider<CalculatorBlocType>(
          create: (context) => CalculatorBloc(repository: context.read()),
          child: const CalculatorScreen(),
        ),
      ),
    );
  }
}
