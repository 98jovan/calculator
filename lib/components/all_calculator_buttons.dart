import 'package:calculator/bloc/calculator_bloc.dart';
import 'package:calculator/components/calculator_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CalculatorButtons extends StatelessWidget {
  const CalculatorButtons({
    super.key,
    required this.calculation,
  });

  final String calculation;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: [
          Row(
            children: [
              CalculatorButton(
                character: '7',
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('7'),
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('8'),
                character: '8',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('9'),
                character: '9',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('/'),
                character: '/',
              )
            ],
          ),
          Row(
            children: [
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('4'),
                character: '4',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('5'),
                character: '5',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('6'),
                character: '6',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('*'),
                character: 'X',
              )
            ],
          ),
          Row(
            children: [
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('1'),
                character: '1',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('2'),
                character: '2',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('3'),
                character: '3',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('-'),
                character: '-',
              )
            ],
          ),
          Row(
            children: [
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('0'),
                character: '0',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('.'),
                character: '.',
              ),
              CalculatorButton(
                onPressed: () {
                  context.read<CalculatorBlocType>().events.fetchData('C');
                  context
                      .read<CalculatorBlocType>()
                      .events
                      .saveHistory(calculation);
                },
                character: 'C',
              ),
              CalculatorButton(
                onPressed: () =>
                    context.read<CalculatorBlocType>().events.fetchData('+'),
                character: '+',
              ),
            ],
          ),
        ],
      ),
    );
  }
}
