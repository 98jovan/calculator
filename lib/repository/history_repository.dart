class HistoryRepository {
  HistoryRepository() {
    history = [];
  }
  late final List<String> history;

  Future<List<String>> addToHistory(String expression) async {
    history.add(expression);
    return history;
  }
}
