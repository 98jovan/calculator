// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:calculator/model/result.dart';
import 'package:calculator/repository/history_repository.dart';
import 'package:calculator/utils/number_parser.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'calculator_bloc.rxb.g.dart';

/// A contract class containing all events of the CalculatorBloC.
abstract class CalculatorBlocEvents {
  void fetchData(String char);
  void saveHistory(String expression);
}

/// A contract class containing all states of the CalculatorBloC.
abstract class CalculatorBlocStates {
  /// The loading state
  Stream<bool> get isLoading;

  /// The error state
  Stream<String> get errors;

  Stream<Results> get calculation;
  Stream<List<String>> get history;
}

@RxBloc()
class CalculatorBloc extends $CalculatorBloc {
  CalculatorBloc({required this.repository});
  final HistoryRepository repository;
  String _calculation = '';
  double _result = 0;
  String result = '';
  bool dotUsed = false;
  @override
  Stream<String> _mapToErrorsState() =>
      errorState.map((error) => error.toString());

  @override
  Stream<bool> _mapToIsLoadingState() => loadingState;

  @override
  Stream<Results> _mapToCalculationState() => MergeStream([
        _$fetchDataEvent.map((event) {
          final RegExp regExp1 = RegExp(r'[-./*+]$');
          if (event == 'C') {
            _calculation = '';
            _result = 0;
          } else if ((_calculation == '' ||
                  _calculation == '+' ||
                  _calculation == '*' ||
                  _calculation == '-' ||
                  _calculation == '/') &&
              (event == '+' || event == '*' || event == "/")) {
            _calculation = '';
          } else if (_calculation.isNotEmpty) {
            if (regExp1.hasMatch(
                  _calculation.substring(_calculation.length - 1),
                ) &&
                regExp1.hasMatch(event)) {
              _calculation =
                  _calculation.substring(0, _calculation.length - 1) + event;
            } else {
              _calculation = _calculation + event;
            }
          } else if (_calculation == '0') {
            _calculation = event;
          } else {
            _calculation = _calculation + event;
          }
          try {
            result = numberParser(_calculation);
            _result = double.parse(result);
          } catch (e) {
            _result = _result;
          }
          return Results(calculation: _calculation, result: _result);
        }),
      ])
          .startWith(Results(calculation: '0', result: 0))
          .shareReplay(maxSize: 1);

  @override
  Stream<List<String>> _mapToHistoryState() => _$saveHistoryEvent
          .debounceTime(const Duration(seconds: 2))
          .switchMap((event) {
        return repository.addToHistory(event).asStream();
      });
}
