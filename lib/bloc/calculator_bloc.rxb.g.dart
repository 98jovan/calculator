// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'calculator_bloc.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class CalculatorBlocType extends RxBlocTypeBase {
  CalculatorBlocEvents get events;
  CalculatorBlocStates get states;
}

/// [$CalculatorBloc] extended by the [CalculatorBloc]
/// {@nodoc}
abstract class $CalculatorBloc extends RxBlocBase
    implements CalculatorBlocEvents, CalculatorBlocStates, CalculatorBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [fetchData]
  final _$fetchDataEvent = PublishSubject<String>();

  /// Тhe [Subject] where events sink to by calling [saveHistory]
  final _$saveHistoryEvent = PublishSubject<String>();

  /// The state of [isLoading] implemented in [_mapToIsLoadingState]
  late final Stream<bool> _isLoadingState = _mapToIsLoadingState();

  /// The state of [errors] implemented in [_mapToErrorsState]
  late final Stream<String> _errorsState = _mapToErrorsState();

  /// The state of [calculation] implemented in [_mapToCalculationState]
  late final Stream<Results> _calculationState = _mapToCalculationState();

  /// The state of [history] implemented in [_mapToHistoryState]
  late final Stream<List<String>> _historyState = _mapToHistoryState();

  @override
  void fetchData(String char) => _$fetchDataEvent.add(char);

  @override
  void saveHistory(String expression) => _$saveHistoryEvent.add(expression);

  @override
  Stream<bool> get isLoading => _isLoadingState;

  @override
  Stream<String> get errors => _errorsState;

  @override
  Stream<Results> get calculation => _calculationState;

  @override
  Stream<List<String>> get history => _historyState;

  Stream<bool> _mapToIsLoadingState();

  Stream<String> _mapToErrorsState();

  Stream<Results> _mapToCalculationState();

  Stream<List<String>> _mapToHistoryState();

  @override
  CalculatorBlocEvents get events => this;

  @override
  CalculatorBlocStates get states => this;

  @override
  void dispose() {
    _$fetchDataEvent.close();
    _$saveHistoryEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}
