import 'package:calculator/bloc/calculator_bloc.dart';
import 'package:calculator/components/all_calculator_buttons.dart';
import 'package:calculator/components/text_container.dart';
import 'package:calculator/model/result.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:provider/provider.dart';

class CalculatorScreen extends StatefulWidget {
  const CalculatorScreen({super.key});

  @override
  State<CalculatorScreen> createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  @override
  Widget build(BuildContext context) {
    var calculation = '';
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: RxBlocBuilder<CalculatorBlocType, Results>(
                state: (bloc) => bloc.states.calculation,
                builder: (context, snapshot, bloc) {
                  calculation =
                      "${snapshot.data?.calculation}=${snapshot.data?.result}";
                  context
                      .read<CalculatorBlocType>()
                      .events
                      .saveHistory(calculation);
                  return snapshot.hasData
                      ? Column(
                          children: [
                            TextConatiner(
                              text: snapshot.data?.calculation ?? '',
                            ),
                            TextConatiner(
                              text: snapshot.data!.result!.toStringAsFixed(
                                snapshot.data!.result!.truncateToDouble() ==
                                        snapshot.data!.result!
                                    ? 0
                                    : 2,
                              ),
                            ),
                          ],
                        )
                      : const SizedBox();
                },
              ),
            ),
            SizedBox(
              height: 200,
              child: RxBlocBuilder<CalculatorBlocType, List<String>>(
                state: (bloc) => bloc.states.history,
                builder: (context, snapshot, bloc) {
                  return ListView.builder(
                    itemCount: snapshot.data?.length ?? 0,
                    itemBuilder: (context, index) => ListTile(
                      leading: const Icon(Icons.history),
                      title: Text(snapshot.data?[index] ?? ''),
                    ),
                  );
                },
              ),
            ),
            CalculatorButtons(calculation: calculation),
          ],
        ),
      ),
    );
  }
}
