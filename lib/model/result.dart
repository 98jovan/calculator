class Results {
  Results({
    required this.calculation,
    required this.result,
  });
  String calculation;
  double? result;
}
